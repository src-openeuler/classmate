Name:          classmate
Version:       1.3.1
Release:       7
Summary:       Zero-dependency Java library
License:       ASL 2.0
Url:           http://github.com/cowtowncoder/java-classmate/
Source0:       https://github.com/cowtowncoder/java-classmate/archive/%{name}-%{version}.tar.gz
BuildArch:     noarch

# https://github.com/FasterXML/java-classmate/commit/80c4619
Patch1:		   backport-Fix-Array-Index-Out-Of-Bounds.patch

BuildRequires: maven-local, mvn(com.fasterxml:oss-parent:pom:), mvn(junit:junit), mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires: mvn(org.apache.maven.plugins:maven-enforcer-plugin)
Provides:      %{name}-javadoc%{?_isa} %{name}-javadoc
Obsoletes:     %{name}-javadoc

%description
ClassMate is a zero-dependency Java library for accurately introspecting type information,
including reliable resolution of generic type declarations for both classes ("types") and
members (fields, methods and constructors).

%prep
%autosetup -n java-%{name}-%{name}-%{version} -p1

find \( -name "*.jar" -o -name "*.class" \) -delete

%pom_remove_plugin :maven-source-plugin
%pom_xpath_remove "pom:plugin[pom:artifactId = 'maven-javadoc-plugin']/pom:executions"

sed -i 's/\r//' src/main/resources/META-INF/LICENSE src/main/resources/META-INF/NOTICE
cp -p src/main/resources/META-INF/NOTICE .
cp -p src/main/resources/META-INF/LICENSE .

rm -rf src/test/java/com/fasterxml/classmate/AnnotationsTest.java

%mvn_file :%{name} %{name}

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc README.md VERSION.txt
%license LICENSE NOTICE
%{_javadocdir}/%{name}/*

%changelog
* Wed Dec 11 2024 zhaosaisai <zhaosaisai@kylinos.cn> - 1.3.1-7
- Backport Fix Array Index Out Of Bounds Exception when resolving Object.class

* Fri Dec 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.3.1-6
- Package init
